`1.0.1`
-------

- **FIX:** workaround for the error on authentication that appeared after latest odoo updates

`1.0.0`
-------

- Init version
